# amberol

Plays music, and nothing else

https://gitlab.gnome.org/World/amberol

<br>
**NOTE:** Do not compile in chroot environment

<br><br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/gnome/amberol.git
```
